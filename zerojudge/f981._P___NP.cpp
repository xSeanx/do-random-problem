#include <stdio.h>
#include <math.h>
using namespace std;
const double eps = 0.000001, sq2 = sqrt(2.00);

struct complex {
    double r, i;
    inline complex operator+ (const complex &ot) const {return {r + ot.r, i + ot.i};}
    inline complex operator- (const complex &ot) const {return {r - ot.r, i - ot.i};}
    inline complex operator- () const {return {-r, -i};}
    inline complex operator* (const complex &ot) const {return {r * ot.r - i * ot.i, r * ot.i + i * ot.r};}
    inline complex operator* (const double &ot) const {return {ot * r, ot * i};}
    inline complex operator/ (const complex &ot) const {
        double nor = ot.r * ot.r + ot.i * ot.i;
        return {(r * ot.r + i * ot.i) / nor, (- r * ot.i + i * ot.r) / nor};
    }
    inline complex operator/ (const double &ot) const {return {r / ot, i / ot};}
    inline double norm() {return sqrt(r * r + i * i);}
    inline bool operator== (const complex &ot) const {return abs(r - ot.r) < eps && abs(i - ot.i) < eps;}
    inline void out() const {
        if (0.005 > r && r > -0.005) printf("0.00");
        else {
            if (r < -eps) putchar('-');
            printf("%.2lf", abs(r));
        }
        if (0.005 > i && i > -0.005) printf("+0.00i");
        else {
            if (i > -eps) putchar('+');
            else putchar('-');
            printf("%.2lfi", abs(i));
        }
    }
    inline bool operator< (const complex &ot) const {
        if (abs(r - ot.r) < eps) {
            return i < ot.i;
        }
        return r < ot.r;
    }
} A, B, C, D, E, F, G, Zero = {0, 0};

int main() {
    while (scanf("%lf%lf%lf%lf%lf%lf", &A.r, &A.i, &B.r, &B.i, &C.r, &C.i) == 6) {
        D = B * B - A * C * 4;
        if (D == Zero) {
            E = - B / A / 2;
            printf("the only x = ");
            E.out();
            putchar('\n');
        } else {
            double r = D.norm();
            if (D.i > 0) {
                if (eps > D.i) E = {sqrt(abs(D.r + D.r)), 0};
                else E = {sqrt(r + D.r), sqrt(r - D.r)};
            } else {
                if (D.i > eps) E = {sqrt(abs(D.r + D.r)), 0};
                else E = {-sqrt(r + D.r), sqrt(r - D.r)};
            }
            E = E / sq2;
            F = (- B + E) / A / 2;
            G = (- B - E) / A / 2;
            if (G < F) E = F, F = G, G = E;
            printf("first x = ");
            F.out();
            printf(", second x = ");
            G.out();
            putchar('\n');
        }
    }
}
